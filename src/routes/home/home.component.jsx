import SearchBox from '../search-box/search-box.component';
import MovieList from '../../components/movie-list/movie-list.component';
import './home.styles.css';
import { data } from '../../data';
import { useEffect, useContext } from 'react';
import { AppContext } from '../../context/app.context';

const Home = () => {
  const {
    searchString,
    handleSearch,
    movies,
    setMovies,
    newFilterMovies,
    filterMovies,
    setFilterMovies,
  } = useContext(AppContext);

  useEffect(() => {
    setMovies(data);
  }, []);

  useEffect(() => {
    setFilterMovies(newFilterMovies);
  }, [movies, searchString]);

  const {currentUser} = useContext(AppContext);
  return (
    <div className='home-container'>
      {currentUser ? (
        <div className='toast toast-top toast-end'>
          <div className='alert alert-success'>
            <div>
              <span className='text-2xl'>Login successfully.</span>
            </div>
          </div>
        </div>
      ) : (
        <></>
      )}
      <SearchBox />
      <MovieList filterMovies={filterMovies} />
    </div>
  );
};

export default Home;
